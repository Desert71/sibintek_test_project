# Install base Python image
FROM python:3.10.14-slim-bullseye

# Set working directory to previously added app directory
WORKDIR /app/

# Copy files to the container
COPY *py /app/

ENTRYPOINT [ "python", "test_program.py"]
